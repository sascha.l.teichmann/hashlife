package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"math"
	"strings"
	"time"
)

func readUniverse(r io.Reader) (*universe, error) {

	start := time.Now()
	defer func() {
		log.Printf("loading took %v.\n", time.Since(start))
	}()

	type pt struct{ x, y int }

	pts := make([]pt, 0, 10_000)

	minX, minY := math.MaxInt32, math.MaxInt32

	if err := readRLE(r, func(x, y int) {
		if x < minX {
			minX = x
		}
		if y < minY {
			minY = y
		}
		pts = append(pts, pt{x, y})
	}); err != nil {
		return nil, err
	}

	t := newTree()
	if len(pts) == 0 {
		return &universe{
			t:    t,
			root: t.createRoot(),
		}, nil
	}

	type bnode [4]addr

	open := make(map[pt]*bnode, len(pts))

	for _, p := range pts {
		p.x -= minX
		p.y -= minY
		k := pt{p.x >> 1, p.y >> 1}
		bn := open[k]
		if bn == nil {
			bn = &bnode{t.dead, t.dead, t.dead, t.dead}
			open[k] = bn
		}
		idx := p.x&1 | ((p.y & 1) << 1)
		bn[idx] = t.alive
	}

	k := uint32(1)

	for len(open) != 1 {
		next := make(map[pt]*bnode, len(open)/4)
		z := t.empty(k)
		for p, v := range open {
			nk := pt{p.x >> 1, p.y >> 1}
			bn := next[nk]
			if bn == nil {
				bn = &bnode{z, z, z, z}
				next[nk] = bn
			}
			idx := p.x&1 | ((p.y & 1) << 1)
			bn[idx] = t.create(v[0], v[1], v[2], v[3])
		}
		k++
		open = next
	}

	var root *bnode
	for _, v := range open {
		root = v
	}

	return &universe{
		t:    t,
		root: t.create(root[0], root[1], root[2], root[3]),
	}, nil
}

func readRLE(r io.Reader, set func(x, y int)) error {

	start := time.Now()
	defer func() {
		log.Printf("reading took %v.\n", time.Since(start))
	}()

	var x, y int
	var arg int

	scanner := bufio.NewScanner(r)
	var bits int
out:
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "x") || strings.HasPrefix(line, "#") {
			continue
		}
		line = strings.TrimSpace(line)
		for _, c := range line {
			var param int
			if arg == 0 {
				param = 1
			} else {
				param = arg
			}
			switch c {
			case 'b':
				x += param
				arg = 0
			case 'o':
				for ; param > 0; param-- {
					set(x, y)
					x++
					bits++
				}
				arg = 0
			case '$':
				y += param
				x = 0
				arg = 0
			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
				arg = 10*arg + int(c-'0')
			case '!':
				break out
			default:
				return fmt.Errorf("illegal char %c in RLE file", c)
			}
		}
	}
	log.Printf("inserted bits set %d.\n", bits)

	return scanner.Err()
}
