package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"
	"time"
)

func main() {

	var (
		images     = flag.Bool("images", false, "Write images")
		profile    = flag.String("c", "", "Write cpu profile to file")
		ffd        = flag.Bool("ffd", false, "Use fast forward mode")
		steps      = flag.Uint64("steps", 10_000, "Steps")
		iterations = flag.Uint64("iterations", 100, "Number of iterations")
	)

	flag.Parse()

	if *profile != "" {
		f, err := os.Create(*profile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	u, err := readUniverse(os.Stdin)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	log.Println(u.stats())
	if *images {
		if err := savePNG(u, "images/000.png"); err != nil {
			log.Printf("warn: %v\n", err)
		}
	}
	for i := uint64(1); i <= *iterations; i++ {
		start := time.Now()
		if *ffd {
			steps := u.FastForward()
			log.Printf("Fast forward took %d steps.\n", steps)
		} else {
			u.Advance(*steps)
		}
		log.Println(u.stats())
		if *images {
			if err := savePNG(u, fmt.Sprintf("images/%03d.png", i)); err != nil {
				log.Printf("warn: %v\n", err)
			}
		}
		log.Printf("Advance/write cycle took %v.\n", time.Since(start))
	}
}
