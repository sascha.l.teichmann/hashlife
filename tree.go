package main

import (
	"fmt"
	"log"
	"math"
	"time"
)

type addr uint32

type node struct {
	nw, ne, sw, se addr
	level          uint32
	population     uint64
}

type cacheKey struct {
	addr  addr
	depth int32
}

const (
	nodeChunkBits = 12
	nodeChunkSize = 1 << nodeChunkBits
)

type tree struct {
	nodes [][nodeChunkSize]node
	free  []addr

	alive addr
	dead  addr
	hash  map[uint64][]addr

	results map[cacheKey]addr
}

type universe struct {
	t    *tree
	root addr

	generations uint64
}

func (a addr) node(t *tree) *node {
	return &t.nodes[a>>nodeChunkBits][a&(nodeChunkSize-1)]
}

func (a addr) nw(t *tree) addr           { return a.node(t).nw }
func (a addr) ne(t *tree) addr           { return a.node(t).ne }
func (a addr) sw(t *tree) addr           { return a.node(t).sw }
func (a addr) se(t *tree) addr           { return a.node(t).se }
func (a addr) level(t *tree) uint32      { return a.node(t).level }
func (a addr) population(t *tree) uint64 { return a.node(t).population }
func (a addr) empty(t *tree) bool        { return a.population(t) == 0 }

func newUniverse() *universe {
	t := newTree()
	return &universe{t: t, root: t.createRoot()}
}

func (u *universe) population() uint64 {
	return u.root.population(u.t)
}

func (u *universe) draw(fn func(x, y int)) {
	u.t.draw(u.root, fn)
}

func (u *universe) extent() (int, int, int, int) {
	return u.t.extent(u.root)
}

func (u *universe) stats() string {

	return fmt.Sprintf(
		"Generation %d population %d",
		u.generations, u.root.population(u.t))
}

func (u *universe) FastForward() uint64 {
	start := time.Now()
	defer func() {
		log.Printf("Fast forward took %v.\n", time.Since(start))
	}()

	//log.Printf("intern size: %d\n", u.t.internSize())
	for u.root.level(u.t) < 3 ||
		u.root.nw(u.t).population(u.t) != u.root.nw(u.t).se(u.t).se(u.t).population(u.t) ||
		u.root.ne(u.t).population(u.t) != u.root.ne(u.t).sw(u.t).sw(u.t).population(u.t) ||
		u.root.sw(u.t).population(u.t) != u.root.sw(u.t).ne(u.t).ne(u.t).population(u.t) ||
		u.root.se(u.t).population(u.t) != u.root.se(u.t).nw(u.t).nw(u.t).population(u.t) {
		u.root = u.t.expandUniverse(u.root)
	}
	steps := uint64(1) << (u.root.level(u.t) - 2)
	log.Printf("Taking a step of %d\n", steps)
	u.root = u.t.shrinkUniverse(u.t.cached(u.root, -1))
	u.generations += steps
	return steps
}

func (u *universe) Advance(steps uint64) {
	start := time.Now()
	defer func() {
		log.Printf(
			"Advancing %d steps took %v.\n",
			steps, time.Since(start))
	}()

	if steps == 0 {
		return
	}

	bits := make([]bool, 0, 64)

	for n := steps; n != 0; n >>= 1 {
		bits = append(bits, n&1 == 1)
		u.root = u.t.expandUniverse(u.root)
	}

	for k := range bits {
		j := len(bits) - k - 1
		if bits[len(bits)-k-1] {
			u.root = u.t.cached(u.root, int32(j))
		}
	}
	u.root = u.t.shrinkUniverse(u.root)
	u.generations += steps
}

func newTree() *tree {

	t := &tree{
		hash:    make(map[uint64][]addr),
		results: make(map[cacheKey]addr),
	}

	dead := t.alloc()
	dead.node(t).population = 0
	t.dead = t.intern(dead)

	alive := t.alloc()
	alive.node(t).population = 1
	t.alive = t.intern(alive)

	return t
}

func (t *tree) cached(node addr, j int32) addr {
	if result, found := t.results[cacheKey{node, j}]; found {
		return result
	}
	result := t.next(node, j)
	t.results[cacheKey{node, j}] = result
	return result
}

func (t *tree) next(node addr, j int32) addr {
	n := node.node(t)
	if n.population == 0 {
		return n.nw
	}
	if n.level == 2 {
		return n.life4x4(t)
	}
	if j < 0 || int32(n.level-2) < j {
		j = int32(n.level - 2)
	}

	nnw := n.nw.node(t)
	nne := n.ne.node(t)
	nsw := n.sw.node(t)
	nse := n.se.node(t)

	c1 := t.cached(t.create(nnw.nw, nnw.ne, nnw.sw, nnw.se), j)
	c2 := t.cached(t.create(nnw.ne, nne.nw, nnw.se, nne.sw), j)
	c3 := t.cached(t.create(nne.nw, nne.ne, nne.sw, nne.se), j)
	c4 := t.cached(t.create(nnw.sw, nnw.se, nsw.nw, nsw.ne), j)
	c5 := t.cached(t.create(nnw.se, nne.sw, nsw.ne, nse.nw), j)
	c6 := t.cached(t.create(nne.sw, nne.se, nse.nw, nse.ne), j)
	c7 := t.cached(t.create(nsw.nw, nsw.ne, nsw.sw, nsw.se), j)
	c8 := t.cached(t.create(nsw.ne, nse.nw, nsw.se, nse.sw), j)
	c9 := t.cached(t.create(nse.nw, nse.ne, nse.sw, nse.se), j)

	if j < int32(n.level-2) {
		return t.create(
			t.create(c1.se(t), c2.sw(t), c4.ne(t), c5.nw(t)),
			t.create(c2.se(t), c3.sw(t), c5.ne(t), c6.nw(t)),
			t.create(c4.se(t), c5.sw(t), c7.ne(t), c8.nw(t)),
			t.create(c5.se(t), c6.sw(t), c8.ne(t), c9.nw(t)))
	}
	return t.create(
		t.cached(t.create(c1, c2, c4, c5), j),
		t.cached(t.create(c2, c3, c5, c6), j),
		t.cached(t.create(c4, c5, c7, c8), j),
		t.cached(t.create(c5, c6, c8, c9), j))
}

func (t *tree) life(nw, n, ne, w, x, e, sw, s, se addr) addr {
	sum := nw.population(t) + n.population(t) + ne.population(t) +
		w.population(t) + e.population(t) +
		sw.population(t) + s.population(t) + se.population(t)
	if sum == 3 || sum == 2 && x.population(t) == 1 {
		return t.alive
	}
	return t.dead
}

func (n *node) life4x4(t *tree) addr {
	nnw := n.nw.node(t)
	nne := n.ne.node(t)
	nsw := n.sw.node(t)
	nse := n.se.node(t)
	nw := t.life(nnw.nw, nnw.ne, nne.nw, nnw.sw, nnw.se, nne.sw, nsw.nw, nsw.ne, nse.nw)
	ne := t.life(nnw.ne, nne.nw, nne.ne, nnw.se, nne.sw, nne.se, nsw.ne, nse.nw, nse.ne)
	sw := t.life(nnw.sw, nnw.se, nne.sw, nsw.nw, nsw.ne, nse.nw, nsw.sw, nsw.se, nse.sw)
	se := t.life(nnw.se, nne.sw, nne.se, nsw.ne, nse.nw, nse.ne, nsw.se, nse.sw, nse.se)
	return t.create(nw, ne, sw, se)
}

func (t *tree) intern(n addr) addr {
	node := n.node(t)
	h := node.hashCode()
	list := t.hash[h]
	if len(list) == 0 {
		t.hash[h] = []addr{n}
		return n
	}
	for _, o := range list {
		if node.equals(o.node(t)) {
			t.recyle(n)
			return o
		}
	}
	t.hash[h] = append(list, n)
	return n
}

/*
func (t *tree) internSize() int {
	var size int
	for _, l := range t.hash {
		size += len(l)
	}
	return size
}
*/

func (n *node) hashCode() uint64 {
	if n.level == 0 {
		return n.population
	}
	return uint64(n.nw)*
		11*uint64(n.ne) +
		101*uint64(n.sw) +
		1007*uint64(n.se)
}

func (n *node) equals(o *node) bool {
	if n.level != o.level {
		return false
	}
	if n.level == 0 {
		return n.population == o.population
	}
	return n.nw == o.nw &&
		n.ne == o.ne &&
		n.sw == o.sw &&
		n.se == o.se
}

func (t *tree) recyle(n addr) {
	t.free = append(t.free, n)
}

func (t *tree) alloc() addr {
	if len(t.free) == 0 {
		base := len(t.nodes) * nodeChunkSize
		t.nodes = append(t.nodes, [nodeChunkSize]node{})
		for i := nodeChunkSize - 1; i >= 0; i-- {
			t.free = append(t.free, addr(base+i))
		}
		//log.Printf("Number of nodes: %d\n", len(t.nodes))
	}

	n := t.free[len(t.free)-1]
	t.free = t.free[:len(t.free)-1]
	return n
}

func (t *tree) create(nw, ne, sw, se addr) addr {
	index := t.alloc()
	node := index.node(t)
	node.nw = nw
	node.ne = ne
	node.sw = sw
	node.se = se
	node.level = nw.level(t) + 1
	node.population = nw.population(t) +
		ne.population(t) +
		sw.population(t) +
		se.population(t)
	return t.intern(index)
}

func (t *tree) empty(level uint32) addr {
	if level == 0 {
		return t.dead
	}
	n := t.empty(level - 1)
	return t.create(n, n, n, n)
}

func (t *tree) expandUniverse(root addr) addr {
	r := root.node(t)
	border := t.empty(r.level - 1)
	return t.create(
		t.create(border, border, border, r.nw),
		t.create(border, border, r.ne, border),
		t.create(border, r.sw, border, border),
		t.create(r.se, border, border, border))
}

func (t *tree) createRoot() addr {
	return t.empty(3)
}

func (t *tree) shrinkUniverse(root addr) addr {
	n := root.node(t)

	depth := n.level

	for depth > 3 {
		nw := n.nw.node(t)
		ne := n.ne.node(t)
		sw := n.sw.node(t)
		se := n.se.node(t)
		if nw.nw.empty(t) && nw.ne.empty(t) && nw.sw.empty(t) &&
			ne.nw.empty(t) && ne.ne.empty(t) && ne.se.empty(t) &&
			sw.nw.empty(t) && sw.sw.empty(t) && sw.se.empty(t) &&
			se.ne.empty(t) && se.sw.empty(t) && se.se.empty(t) {
			depth--
			root = t.create(nw.se, ne.sw, sw.ne, se.nw)
			n = root.node(t)
		} else {
			break
		}
	}
	return root
}

func (t *tree) draw(root addr, fn func(x, y int)) {
	var recurse func(n *node, x, y int)
	recurse = func(n *node, x, y int) {
		if n.level == 0 {
			if n.population > 0 {
				fn(x, y)
			}
			return
		}
		half := 1 << (n.level - 1)
		if nw := n.nw.node(t); nw.population > 0 {
			recurse(nw, x, y)
		}
		if ne := n.ne.node(t); ne.population > 0 {
			recurse(ne, x+half, y)
		}
		if sw := n.sw.node(t); sw.population > 0 {
			recurse(sw, x, y+half)
		}
		if se := n.se.node(t); se.population > 0 {
			recurse(se, x+half, y+half)
		}
	}
	recurse(root.node(t), 0, 0)
}

func (t *tree) extent(root addr) (minX, minY, maxX, maxY int) {
	minX, maxX = math.MaxInt32, math.MinInt32
	minY, maxY = math.MaxInt32, math.MinInt32
	t.draw(root, func(x, y int) {
		if x < minX {
			minX = x
		}
		if x > maxX {
			maxX = x
		}
		if y < minY {
			minY = y
		}
		if y > maxY {
			maxY = y
		}
	})
	return
}
