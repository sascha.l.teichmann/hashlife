package main

import (
	"compress/gzip"
	"log"
	"math"
	"os"
	"runtime"
	"sync"
	"testing"
	"time"
)

type simpleLife struct {
	width  int
	height int
	cells  []byte
	next   []byte
}

func newSimpleLife(width, height int) *simpleLife {
	size := width * height
	return &simpleLife{
		width:  width,
		height: height,
		cells:  make([]byte, size),
		next:   make([]byte, size),
	}
}

func (s *simpleLife) set(x, y int) {
	s.cells[y*s.width+x] = 1
}

func (s *simpleLife) get(x, y int) byte {
	return s.cells[y*s.width+x]
}

func (s *simpleLife) rows(
	wg *sync.WaitGroup,
	rows <-chan int,
	population *uint64,
) {
	defer wg.Done()
	var pop uint64
	for row := range rows {

		rx := row * s.width
		ax := rx - s.width
		bx := rx + s.width

		a := s.cells[ax : ax+s.width]
		r := s.cells[rx : rx+s.width]
		b := s.cells[bx : bx+s.width]
		d := s.next[rx+1 : rx+s.width-1]

		for j, c := range r[1 : len(r)-1] {
			sum := a[j] + a[j+1] + a[j+2] +
				r[j] + /* c */ r[j+2] +
				b[j] + b[j+1] + b[j+2]
			if sum == 3 || sum == 2 && c == 1 {
				d[j] = 1
				pop++
			}
		}
	}
	*population = pop
}

func (s *simpleLife) extent() (minX, minY, maxX, maxY int) {

	minX, maxX = math.MaxInt32, math.MinInt32
	minY, maxY = math.MaxInt32, math.MinInt32

loopMinY:
	for i := 0; i < s.height; i++ {
		for _, c := range s.cells[i*s.width : (i+1)*s.width] {
			if c == 1 {
				minY = i
				break loopMinY
			}
		}
	}

	if minY == math.MaxInt32 {
		return
	}

loopMaxY:
	for i := s.height - 1; i > minY; i-- {
		row := s.cells[i*s.width : (i+1)*s.width]
		for j := len(row) - 1; j >= 0; j-- {
			if row[j] == 1 {
				maxY = i
				break loopMaxY
			}
		}
	}

	minX, maxX = s.width, 0

	for i := minY; i <= maxY; i++ {
		row := s.cells[i*s.width : (i+1)*s.width]
		for j := 0; j < minX; j++ {
			if row[j] == 1 {
				minX = j
				break
			}
		}
		for j := s.width - 1; j > maxX; j-- {
			if row[j] == 1 {
				maxX = j
				break
			}
		}
	}

	return
}

func (s *simpleLife) step() uint64 {
	rows := make(chan int)
	var wg sync.WaitGroup
	n := runtime.NumCPU()
	pop := make([]uint64, n)
	for i := 0; i < n; i++ {
		wg.Add(1)
		i := i
		go s.rows(&wg, rows, &pop[i])
	}
	// Ignore first and last row.
	for row := 1; row < s.height-1; row++ {
		rows <- row
	}
	close(rows)
	wg.Wait()
	var population uint64
	for _, p := range pop {
		population += p
	}
	// swap buffers
	for i := range s.cells {
		s.cells[i] = 0
	}
	s.cells, s.next = s.next, s.cells
	return population
}

func TestSimple(t *testing.T) {
	f, err := os.Open("examples/hexadecimal.rle.gz")
	if err != nil {
		t.Fatalf("error: %v\n", err)
	}
	defer f.Close()
	z, err := gzip.NewReader(f)
	if err != nil {
		t.Fatalf("error: %v\n", err)
	}
	u, err := readUniverse(z)
	if err != nil {
		t.Fatalf("error: %v\n", err)
	}

	minX, minY, maxX, maxY := u.extent()
	width := maxX - minX

	height := maxY - minY

	// An universe expanse max 1 cell in each direction in
	// one generation. So we border it with 13 cells on
	// each edge to be big enough for 13 generations plus
	// one for ease of evaluation.
	simple := newSimpleLife(width+2*(13+1), height+2*(13+1))

	u.draw(func(x, y int) {
		simple.set(x-minX+1+13, y-minY+1+13)
	})

	start := time.Now()
	// Advance 13 generations.
	var spop uint64
	for i := 0; i < 13; i++ {
		spop = simple.step()
	}
	log.Printf("simple took: %v\n", time.Since(start))

	sMinX, sMinY, sMaxX, sMaxY := simple.extent()

	u.Advance(13) // 8 + 4 + 1

	log.Printf("simple population: %d\n", spop)
	log.Printf("hashlife population: %d\n", u.population())

	if spop != u.population() {
		t.Errorf("population is %d, want %d", u.population(), spop)
	}

	hMinX, hMinY, hMaxX, hMaxY := simple.extent()

	sWidth, sHeight := sMaxX-sMinX+1, sMaxY-sMinY+1
	hWidth, hHeight := hMaxX-hMinX+1, hMaxY-hMinY+1

	log.Printf("simple width: %d height: %d \n", sWidth, sHeight)
	log.Printf("hash width: %d height: %d \n", hWidth, hHeight)

	if sWidth != hWidth {
		t.Errorf("width %d, want %d", hWidth, sWidth)
	}

	if sHeight != hHeight {
		t.Errorf("height %d, want %d", hHeight, sHeight)
	}

	var wrong, counted uint64

	u.draw(func(x, y int) {
		counted++
		if simple.get(x-hMinX+sMinX+13+1, y-hMinY+sMinY+13+1) != 1 {
			wrong++
		}
	})

	if counted != u.population() {
		t.Errorf("counted %d, want %d", counted, u.population())
	}

	if wrong != 0 {
		t.Errorf("wrong %d, want 0", wrong)
	}

}
