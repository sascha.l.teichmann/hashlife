package main

import (
	"bufio"
	"errors"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"log"
	"os"
	"time"
)

func savePNG(u *universe, path string) error {
	start := time.Now()
	defer func() {
		log.Printf("writing %s took %v\n", path, time.Since(start))
	}()

	minX, minY, maxX, maxY := u.extent()

	black := color.RGBA{0x00, 0x00, 0x00, 0xff}
	white := color.RGBA{0xff, 0xff, 0xff, 0xff}

	pal := color.Palette{black, white}

	var img *image.Paletted

	const tooLarge = 100_000

	switch {
	case minX >= maxX || minY >= maxY:
		log.Println("warn: universe is empty")
		img = image.NewPaletted(image.Rect(0, 0, 1, 1), pal)
	case maxX-minY > tooLarge || maxY-minY > tooLarge:
		return errors.New("universe too large")
	default:
		img = image.NewPaletted(image.Rect(0, 0, maxX-minX+1, maxY-minY+1), pal)
	}

	draw.Draw(img, img.Rect, image.NewUniform(black), image.ZP, draw.Src)

	u.t.draw(u.root, func(x, y int) {
		img.Pix[img.PixOffset(x-minX, y-minY)] = 1
	})

	f, err := os.Create(path)
	if err != nil {
		return err
	}
	out := bufio.NewWriter(f)

	enc := png.Encoder{
		CompressionLevel: png.BestCompression,
	}

	if err := enc.Encode(out, img); err != nil {
		f.Close()
		return err
	}

	err = out.Flush()

	if err2 := f.Close(); err2 != nil && err == nil {
		err = err2
	}

	return err
}
